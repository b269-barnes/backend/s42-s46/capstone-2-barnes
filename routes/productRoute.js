const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js")


// [RETRIEVE PRODUCTS]
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});


// [RETRIEVE SINGLE PRODUCT]
router.get("/:id", (req, res) => {
	productController.findProduct(req.params.id).then(resultFromController => res.send(resultFromController));
});


// [CREATE PRODUCT]
router.post("/create-product", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.createProduct(verifiedId, req).then(resultFromController => res.send(resultFromController));
});

// [RETRIEVE ACTIVE PRODUCTS]
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => {
		res.send(resultFromController)
	})
});



// [UPDATE PRODUCT]
router.put("/:id/update", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.updateProduct(verifiedId, req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

// [ARCHIVE PRODUCT]
router.patch("/:id/archive", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.archiveProduct(verifiedId, req.params.id).then(resultFromController => res.send(resultFromController));
});

// [ARCHIVE PRODUCT]
router.patch("/:id/unarchive", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.unarchiveProduct(verifiedId, req.params.id).then(resultFromController => res.send(resultFromController));
});

// [DELETE PRODUCT]
router.delete("/:id/delete-product", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.deleteProduct(verifiedId, req.params.id).then(resultFromController => res.send(resultFromController));
});



module.exports = router;
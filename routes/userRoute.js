const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// [EMAIL CHECKER]
router.post("/checkEmail", (req,res) => {
  userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// |USER REGISTRATION|
router.post("/register", (req, res) => {
  userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// [// [USER'S AUTHENTICATION]]
router.post("/login", (req, res) => {
  userController.login(req.body).then(resultFromController => res.send(resultFromController));
});

// |RETRIEVE USERS|
router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


// [RETRIEVE USER]
router.get("/profile", auth.verify, (req, res) => {
  userData = auth.decode(req.headers.authorization).id
  userController.getProfile(userData).then(resultFromController => res.send(resultFromController));
});

// [SET USER AS ADMIN]

router.put("/:id/set-as-admin", auth.verify, (req, res) => {
  userData = auth.decode(req.headers.authorization).id
  userController.makeAdmin(userData, req.params.id).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");

// [RETRIEVE ORDERS]
router.get("/", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	orderController.getAllOrders(verifiedId).then(result => res.send(result))
})

// [CHECKOUT FOR NON-ADMIN]
router.post("/checkout", async (req, res) => {
  try {
    const order = await orderController.checkout(req.body);
    res.status(201).json(order);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});



// [RETRIEVE AUTHENTICATED USER'S ORDER] 
router.get("/user-orders", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	orderController.getUserOrders(verifiedId).then(result => res.send(result))
});

// [DELETE ORDERS]

router.delete("/:id/delete-order", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	orderController.deleteOrder(verifiedId, req.params.id).then(result => res.send(result))
})

module.exports = router;
const auth = require("../auth.js")
const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");

// [RETRIEVE ORDERS]

module.exports.getAllOrders = (data) => {
	return User.findById(data).then((result, err) => {
		if(result.isAdmin == false) {
			return "Sorry, you need admin permissions to access this request"
		} else {
			return Order.find().then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
};

// [CHECKOUT FOR NON-ADMIN]


module.exports.checkout = async (data) => {
  const { userId, productId, quantity } = data;

  try {
    const user = await User.findById(userId);

    if (user && user.isAdmin) {
      throw new Error("Admins are not allowed to checkout.");
    }

    const product = await Product.findById(productId);

    if (!product) {
      throw new Error("Product not found.");
    }

    const subtotal = quantity * product.price;

    const newOrder = new Order({
      user: user._id,
      totalAmount: subtotal,
      items: [
        {
          name: product._id,
          price: product.price,
          quantity: quantity,
          subTotal: subtotal,
        },
      ],
    });

    const savedOrder = await newOrder.save();

    // Populate the `fullName` field of the `user` object
    const populatedOrder = await Order.findById(savedOrder._id).populate('user').populate('items.name', 'name');

    return populatedOrder;
  } catch (error) {
    throw new Error(error.message);
  }
};


// [RETRIEVE ALL ORDER] 
module.exports.getUserOrders = (data) => {
	return User.findById(data).then((result, err) => {
		if(err) {
			return false
		} else {
			return Order.find().then((result, err) => {
				if(err) {
					return false 
				} else {
					return result
				}
			})
		}
	});
};

// [DELETE ORDER]
module.exports.deleteOrder = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(err) {
			return false
		} else {
			return Order.findByIdAndDelete(data).then((result, err) => {
				if(err) {
					return false
				} else {
					return true
				}
			})
		}
	})
}

// [ADD ITEM TO CART]


const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
	{
		user: {
		  type: mongoose.Schema.Types.ObjectId,
		  ref: "User",
		  required: true
		},
		totalAmount: {
			type: Number,
			default: 0
		},
		purchasedOn: {
			type: Date, 
			default: new Date()
		},
		items: [
			{
				name: {
				        type: mongoose.Schema.Types.ObjectId,
				        ref: "Product",
				        required: true
				 
				},
				price: {
					type: Number,
				     ref: "Product",
					required: [true, "Price is required"]
				},
				quantity: {
					type: Number,
					default: 0
				},
				subTotal : {
					type : Number,
					default : 0
				},
				orderedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
);

module.exports = mongoose.model("Order", orderSchema);

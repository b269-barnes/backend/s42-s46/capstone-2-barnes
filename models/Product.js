	const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Please include product's name."]
		}, 
		description: {
			type: String,
			required: [true, "Please include product's description"]
		}, 
		category : {
			type: String,
			required : [true, "Please include product's category"]
		},
		price: {
			type: Number,
			required: [true, "Please include product's price"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		stock: {
			type: Number,
			default: 0,
			required: [true, "Out of stock"]
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	}
);

module.exports = mongoose.model("Product", productSchema);